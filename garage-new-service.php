<?php include 'header.php';?>

<div class="container service-dashboard">
	<div class="row">
	<div class="col s12 m6 service-details">
		<h5 class="col s12 light ">Easy service for BMW 320d 2004</h5>
		<ul class="collection">
			 <li class="collection-item"><i class="material-icons"></i>Date: 21. June</li>
			 <li class="collection-item"><i class="material-icons"></i>Owner: Alex</li>
		</ul>
		<p>Need to be done:</p>
		<ul class="collection">
	       <li class="collection-item"><i class="material-icons">done</i> Oil change</li>
	       <li class="collection-item"><i class="material-icons">done</i> Oil change</li>
				 <li class="collection-item"><i class="material-icons">done</i> Oil change</li>
				 <li class="collection-item"><i class="material-icons">done</i> Oil change</li>
				 <li class="collection-item"><i class="material-icons">done</i> Oil change</li>
				 <li class="collection-item"><i class="material-icons">done</i> Oil change</li>
	  </ul>
		<div class="no-padding col s12">
			<a class="btn make-offer-trigger right" href="#make-offer"><span>Answer</span></a>
		</div>
	</div>
	<?php include 'messanger.php';?>
	</div>
</div>
<!-- Make offer modal -->
<div class="row">
   <div id="make-offer" class="modal s12">
    <div class="col s12 modal-content">
     <h4>Easy service for BMW 320d 2004</h4>
    <p>Make your offer...</p>
		</div>
		<div class="col s12 divider"></div>
		<div class="col s12 modal-content">
			<div class="input-field"><input id="price" type="number"><label for="price">Price</label></div>
			<div class="input-field"><input id="date" class="datepicker" type="text"><label for="date">Pick a date</label></div>
			<div class="input-field"><textarea id="text1" class="materialize-textarea"></textarea><label for="text1">Note</label></div>
			<div class="btn col s6 btn-flat left">Reject</div>
			<div class="btn col s6 right">Send offer</div>
		 </div>
   </div>
</div>
<div id="floating-contant-btn" class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
	<a href="#feedback-form-wrapper"class="btn-floating btn-large">
  	<i class="fa fa-envelope-o" aria-hidden="true"></i>
  </a>
</div>
<div id="floating-contant-btn" class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
	<a href="#feedback-form-wrapper"class="btn-floating btn-large">
  	<i class="fa fa-envelope-o" aria-hidden="true"></i>
  </a>
</div>
<?php include 'footer.php';?>
