<?php include 'header.php';?>

  <div class="parallax-container">
    <div class="parallax"><img src="images/Team_Carhelper.ch_Auto_Service.jpg" alt=""></div>

  </div>

	<!-- Mission -->
<div class="container mission white z-depth-1 row" >
	<h6 class="icon-circle-compass center" style="font-size: 56px"></h6>
	<h5 class="light center">Unsere Mission</h5>
	<p class="light full-text">Hattest du schon mal ein negatives Erlebnis bei einem Garagenbesuch? Sehr wahrscheinlich schon. Dies kommt oft daher, dass Preise vorher nicht in Erfahrung gebracht werden können oder mehr Leistungen erbracht oder verrechnet werden, welche Kunden nicht erwarten.
		Arbeiten, welche für Mechaniker völlig logisch und nötig sind, stossen bei Autohalter oft auf Unverständnis. Dieser technische Informationsvorsprung von Garagen gegenüber ihren Kunden sorgt daher immer wieder für Missverständnisse und Unmut - besonders bei der Rechnungsstellung.
		Mit Carhelper wollen wir diese Missverständnisse ausräumen und dem Kunden mehr Vertrauen und Sicherheit in der Garagenwahl geben.
		Unser oberstes Ziel ist es daher, ein positives Erlebnis für den dich und den Garagisten zu schaffen, indem wir dich zur für dich perfekten Garage führen. Du erhält online 3 Offerten, natürlich unverbindlich und kostenlos und entscheidest dich für den besten Deal.</p>
</div>

<!-- Section 1 feature -->
<div class="container row">
	<h6 class="icon-linegraph center" style="font-size: 56px"></h6>
	<h5 class="light center">Über Carhelper</h5>

	<p class="light full-text">Carhelper wurde im Rahmen eines Start-Up Accelerator Programm der Swiss Start-Up Factory im Januar 2016 ins Leben gerufen. In diesem Programm wurde Carhelper von einer Idee zu einem Prototypen entwickelt, welcher mit seinem Service Kunden auf Anhieb begeisterte. Nach dem erfolgreichen Abschluss des Accelerator wurde im Juni 2016 das Start-Up Carhelper AG gegründet.</p>
</div>

<div class="divider"></div>

	<!-- Team -->
	<div class="container">
		<h6 class="icon-lightbulb center" style="font-size: 56px"></h6>
		<h5 class="light center">Das Gründerteam von Carhelper:</h5>

		<div class="row">
			<div class="col s12 m6 center  team-member">
				<div class="col s12">
					<img class="responsive-img circle col s4 offset-s4" src="images/Matthias_Gerber_Carhelper.ch_auto_service.jpg">
				</div>
				<h4 class="col s12 thin">Matthias Gerber</h4>
				<p>CEO & Founder @ Carhelper AG</p>
				<p class="full-text thin">Matthias war schon immer ein Autonarr. Während seine Kollegen in der Kindheit Disney Comics und Lucky Luke lasen, verschlang Matthias Autoheftlis und wurde zu einem kleinen, wandelnden Autolexikon. Nur logisch, dass er eine Berufslehre als Automechaniker einem Studium vorzog, da er sein Hobby zum Beruf machen wollte. Vor Carhelper arbeitete Matthias in diversen Marketingagenturen und zuletzt als Digital Marketing Manager bei der AMAG. Die Grundidee zu Carhelper hatte Matthias bereits vor Jahren, da er in seinem Umfeld immer wieder viel Frust und Ärger wegen intransparenten und hohen Servicekosten feststellte.</p>
				<span class="about-us-contact-icon icon-linkedin" style="font-size: 36px"></span>
			</div>
			<div class="col s12 m6 center  team-member">
				<div class="col s12">
					<img class="responsive-img circle col s4 offset-s4" src="images/Aleksandar_Stevanovic_Carhelper.ch_auto_service.jpg">
				</div>
				<h4 class="col s12 thin">Aleksandar Stevanovic</h4>
				<p>CTO & Founder @ Carhelper AG</p>
				<p class="full-text thin">Als Aleksandar ein Kind war, verliebte er sich schnell in die Technologie der 90er Jahre. Es begann mit Video Konsolen und dann folgten schnell die Computer - was ihn zum Web Development führte. Er schloss sein ICT Studium in Belgrad ab und machte seine Erfahrungen mit php Development in der führenden Belgrader Software Firma Go Live. Nach ein paar Jahren führte ihn sein Weg in die Schweiz, wo er Matt kennenlernte und nach viertelstündiger Überzeugungsarbeit, entschloss er sich bei Carhelper einzusteigen.</p>
				<span class="about-us-contact-icon icon-linkedin" style="font-size: 36px"></span>
			</div>
		</div>

		<div class="row">
			<div class="col s12 m6 center team-member">
				<div class="col s12">
					<img class="responsive-img circle col s4 offset-s4" src="images/max_meister_carhelper.ch_auto_service.jpg">
				</div>
				<h4 class="col s12 thin">Max Meister</h4>
				<p>Mitglied des Verwaltungsrates @ Carhelper AG</p>
				<p class="full-text thin">Max wurde schon früh mit dem Autofieber infiziert. Sein Vater ist in früheren Jahren erfolgreich Autorennen gefahren und Klein-Maxe durfte jeweils nach den Trainings auf Strecken wie Monza oder Hockenheim mitfahren. Deshalb freut es ihn umso mehr, bei Carhelper das Team tatkräftig unterstützen zu können. Max ist Mitgründer bei der Swiss Start Up Factory und verantwortlich für das Accelerator Programm.</p>
				<span class="about-us-contact-icon icon-linkedin" style="font-size: 36px"></span>
			</div>
			<div class="col s12 m6 center  team-member">
				<div class="col s12">
					<img class="responsive-img circle col s4 offset-s4" src="images/roger-dal-santo-carhelper.ch-auto-service.jpg">
				</div>
				<h4 class="col s12 thin">Roger Dal Santo</h4>
				<p>Mentor @ Carhelper AG</p>
				<p class="full-text thin">Zwischen 1990 und 2000 war Roger Dal Santo als Geschäftsleitungsmitglied für die internationale Expansion der Eurotax-Gruppe in 25 Länder zuständig. Dabei entwickelte er das Eurotax Bewertungssystem vom reinen Print-Anbieter zum Datenlieferanten der Automobilbranche. Nach erfolgreichem Verkauf der Eurotax-Gruppe im Jahre 2001 an Private-Equity-Investoren, investierte Roger Dal Santo in die Aktivitäten der Car4you (Holding) AG - ein Kleinanzeigenportal für Autos im Internet - und entwickelte das Geschäft in 5 europäischen Ländern. 2012 wurde Car4you an verschiedene Medienunternehmen in Europa verkauft und in deren Geschäftseinheiten integriert. Roger Dal Santo studierte Betriebsökonomie an der HWV in Zürich und ist Vater von zwei erwachsenen Kindern.</p>
				<span class="about-us-contact-icon icon-linkedin" style="font-size: 36px"></span>
			</div>
		</div>
	</div>

	<div class="divider"></div>

	<!-- Section Contact -->
	<div class="container row" id="feedback-form-wrapper">
		<h5 class="center col s12 thin no-padding">Was können wir verbessern? Wir freuen uns auf dein Feedback.</h5>
		<div class="col s12  no-padding">
			<div class="col s4 offset-s4 divider blue-grey"></div>
			<p class="col s12 light center  no-padding">Dieser Service ist neu und in ständiger Weiterentwicklung. Wir bitten um Verständnis, falls sich kleine Unannehmlichkeiten einschleichen. Falls du Anregungen oder Fragen hast, teile uns deine Gedanken mit.</p>
			<form id="feedback-form" class="col s12">
				<div class="row">
					<div class="input-field col s12">
						<input id="email" type="email" class="validate">
						<label for="email">Email</label>
					</div>
					<div class="input-field col s12">
						<textarea id="textarea1" class="materialize-textarea"></textarea>
						<label for="textarea1">Sag uns deine Meinung...</label>
					</div>
				</div>
			</form>
			<div class="col s12">
				<div id="feedback-btn"class=" col s6 offset-s3 btn waves-effect waves-light light btn-line">Feedback abgeben</div>
			</div>
		</div>
	</div>


<?php include 'footer.php';?>
