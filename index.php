<?php include 'header.php';?>

		<!-- Service options and form -->
		<div class="mobile-form services-wrapper  valign-wrapper">
			<div class="container row">
				<div class="service-options-header">
					<h5 id="landing-page-text"class="header center col s12 thin white-text">You miss transparency in car servicing?<br><br>Get best offers form 6000 garages.</h5>
					<div onclick="toggleServiceOptions()" class="open-form-btn btn col m4 offset-m4 s12 waves-effect waves-light">Get 3 offers for servicing</div>
				</div>
				<div class="service-options">
					<h5 id="service-options-header-text"class="center col s12 thin white-text">Choose service you need:</h5>
					<!-- Service options panels -->
					<!-- Easy service -->
					<div id='1' class="col s12 m4 service-options-item">
						<div class="card">
							<div class="card-content">
								<div class="service-name">
									<h5 class="center card-icon icon-service-1"></h5>
									<h4 class="thin center card-title">Easy service</h4>
									<p class="center"><a hrer="">Why to choose Easy service?</a></p>
								</div>
								<div class="servicinfo">
									<p><i class="tiny material-icons">done</i> Regulärer Service</p>
									<p><i class="tiny material-icons">done_all</i> Frühlings-Check</p>
									<p><i class="tiny material-icons">done_all</i> Klima Service</p>
									<p><i class="tiny material-icons">done_all</i> Innenreinigung</p>
									<p>...and more</p>
								</div>
							</div>
							<div id="service-1" onclick="toggleServiceForm('1')"class="pick-service waves-effect card-action"><a>Get Easy service</a></div>
						</div>
					</div>
					<!-- Official service -->
					<div class="service-options-item  col s12 m4">
						<div class="card">
							<div class="card-content">
								<div class="service-name">
									<h5 class="center card-icon icon-service-2"></h5>
									<h4 class="thin center card-title">Official service</h4>
									<p class="center">Why to choose Official service?</p>
								</div>
								<div class="servicinfo">
									<p>Text about official services.<br>Text about official services.<br>Text about official services.<br>ext about official services. Text about<br> official services. Text about official services.</p>
								</div>
							</div>
							<div onclick="toggleServiceForm('2')" class="waves-effect card-action"><a>Get Official service</a></div>
						</div>
					</div>
					<!-- Custom service -->
					<div class="service-options-item  col s12 m4">
						<div class="card">
							<div class="card-content">
								<div class="service-name">
									<h5 class="center card-icon icon-service-3"></h5>
									<h4 class="thin center card-title">Custom service</h4>
									<p class="center">Why to choose Custom service?</p>
								</div>
								<div class="servicinfo">
									<p><i class="tiny material-icons">done_all</i> Regulärer Service</p>
									<p><i class="tiny material-icons">done_all</i> Frühlings-Check</p>
									<p><i class="tiny material-icons">done_all</i> Klima Service</p>
									<p><i class="tiny material-icons">done_all</i> Innenreinigung</p>
									<p><i class="tiny material-icons">done_all</i> Inkl. Ersatzwagen</p>
								</div>
							</div>
							<div onclick="toggleServiceForm('3')" class="waves-effect card-action"><a>Get Custom service</a></div>
						</div>
					</div>
				</div>
				<!-- End of Service options panels -->
				<!-- Service form -->
				<form id="service-form" class="card service-form" step="1">
					<div class="service-form-header">
						<h5 class="light">
						<span class="icon-"></span><span class="title"></span></h5>
						<i onclick="closeForm()"class="close-btn icon-close"></i>
					</div>
					<!-- Service form step one - Easy service works -->
          <div class="service-form-content">
            <div class="col  card-content service-form-step one easy">
  						<h6 class="service-form-label">Included works:</h6>
  						<p><input disabled class="included-service" type="checkbox" id="check1" checked="checked" /><label for="check1">Air filter change</label></p>
  						<p><input disabled class="included-service" type="checkbox" id="check2" checked="checked" /><label for="check2">Air filter change</label></p>
  						<p><input disabled class="included-service" type="checkbox" id="check3" checked="checked" /><label for="check3">Air filter change</label></p>
  						<p><input disabled class="included-service" type="checkbox" id="check4" checked="checked" /><label for="check4">Air filter change</label></p>
  						<p><input disabled class="included-service" type="checkbox" id="check5" checked="checked" /><label for="check5">Air filter change</label></p>
  					</div>
						<!-- Service form step one - Official service works -->
						<div class="col  card-content service-form-step one official">
  						<h6 class="service-form-label">Included works:</h6>
  						<p><input disabled class="included-service" type="checkbox" id="check1" checked="checked" /><label for="check1">Air filter change</label></p>
  						<p><input disabled class="included-service" type="checkbox" id="check2" checked="checked" /><label for="check2">Air filter change</label></p>
  					</div>
						<!-- Service form step one - Custom service works -->
						<div class="col  card-content service-form-step one custom">
  						<h6 class="service-form-label">Included works:</h6>
  						<p><input disabled class="included-service" type="checkbox" id="check1" checked="checked" /><label for="check1">Air filter change</label></p>
  						<p><input disabled class="included-service" type="checkbox" id="check2" checked="checked" /><label for="check2">Air filter change</label></p>
  						<p><input disabled class="included-service" type="checkbox" id="check3" checked="checked" /><label for="check3">Air filter change</label></p>
  						<p><input disabled class="included-service" type="checkbox" id="check4" checked="checked" /><label for="check4">Air filter change</label></p>
  						<p><input disabled class="included-service" type="checkbox" id="check5" checked="checked" /><label for="check5">Air filter change</label></p>
  						<h6 class="service-form-label">Extra works:</h6>
  						<p><input name="group1" type="radio" id="radio1" /><label for="radio1">Option 1</label></p>
							<p><input name="group1" type="radio" id="radio2" /><label for="radio2">Option 2</label></p>
							<p><input name="group1" type="radio" id="radio3" /><label for="radio3">Option 3</label></p>
							<p><span class="switch"><label><input type="checkbox"><span class="lever"></span></label></span> Clean car</p>
							<p><input type="checkbox" id="oilchanges" /><label for="oilchanges">Oil changes</label></p>
  						<p><input disabled type="checkbox" id="check6" checked="checked" /><label for="check6">Air filter change</label></p>
  						<div class="input-field">
  							<textarea id="text1" class="materialize-textarea"></textarea><label for="text1">Even more:</label>
  						</div>
  					</div>


  					<!-- Service form step two - enter info -->
  					<div class="col  card-content service-form-step two">
  						<h6 class="service-form-label">About car:</h6>
  						<div class="file-field input-field">
  							<div class="btn blue-grey"><span>Upload car papers</span><input type="file" multiple></div>
  							<div class="file-path-wrapper"><input class="file-path validate" type="text" placeholder="Upload one or more files"></div>
  						</div>
  						<div class="input-field"><input id="kilometrage" type="number"><label for="kilometrage">Kilometrage</label></div>
  						<div class="input-field"><input id="date" class="datepicker" type="text"><label for="date">Pick a date</label></div>
							<h6 class="service-form-label">Contact:</h6>
							<div class="input-field"><input id="name" type="text" class="validate"><label for="name">Your name</label></div>
  						<div class="input-field"><input id="email" type="email" class="validate"><label for="email">Your email</label></div>
  						<div class="input-field"><input id="tel" type="tel" class="validate"><label for="tel">Your phone</label></div>
							<div class="input-field"><input id="address" type="text" class="validate"><label for="address">Your address</label></div>
  						<div class="input-field"><textarea id="text" class="materialize-textarea"></textarea><label for="text">Note</label></div>
  					</div>
  					<!-- Service form step three - show results -->
  					<div class="col  card-content service-form-step three">
  						<h6 class="service-form-label">Garages</h6>
  						<div class="auto-pick">
  							<h5 class="center card-icon icon-genius"></h5>
  							<h5 class="center">Let Carhelper pick garage for you</h5>
  							<p class="center">Tell me why? Lorem ipsum dolor sit amet</p>
								<p class="garage-list-toggle center">I want to choose myself</p>
  						</div>
  						<div class="user-pick hide">
  							<h5 class="center">Choose garages:</h5>
								<div class="divider"></div>
  							<div class="col">
  								<span>Show: </span><a class='dropdown-button white btn waves-effect waves-orange btn-flat' href='#' data-activates='dropdown1'>Official garages</a>
  								<ul id='dropdown1' class='dropdown-content'>
  									<li><a href="#!">Preferred garages</a></li>
  									<li class="divider"></li>
  									<li><a href="#!">All garages</a></li>
  								</ul>
  							</div>
  							<div class="col">
  								<span>in distance of: </span><a class='dropdown-button white btn waves-effect waves-orange btn-flat' href='#' data-activates='dropdown2'>10 km</a>
  								<ul id='dropdown2' class='dropdown-content'>
  									<li><a href="#!">20 km</a></li>
  									<li class="divider"></li>
  									<li><a href="#!">30 km</a></li>
  								</ul>
  							</div>
								<div class="divider col s12"></div>
  							<ul class="collection col s12 no-padding">
  								<li class="collection-item avatar garage">
  									<img src="images/amag_profile_logo.png" alt="" class="circle">
  									<h5 class="title">Bosch garage services</h5>
										<div>
										<img class="car-maker tooltipped" data-position="top" data-delay="10" data-tooltip="Audi" src='images/car-logos/f7387b5adee5d49ffde1d71e89762d97.png'>
										<img class="car-maker tooltipped" data-position="top" data-delay="10" data-tooltip="Audi" src='images/car-logos/11e1627e09b2ad999e782f4cdaa095d7.png'>
										<img class="car-maker tooltipped" data-position="top" data-delay="10" data-tooltip="Audi" src='images/car-logos/0943130931c50a72d0483f17a88f6072.png'>
									</div>
										<p class="address">Address, Address, Address </p>
  									<a href="#!" class="secondary-content">
											<i class="tiny material-icons">gradegradegradegradegrade</i><br>
												<p>24 km away</p>
										</a>
  								</li>
									<li class="collection-item avatar garage">
  									<img src="images/amag_profile_logo.png" alt="" class="circle">
  									<h5 class="title">Bosch garage services</h5>
										<div>
										<img class="car-maker tooltipped" data-position="top" data-delay="10" data-tooltip="Audi" src='images/car-logos/f7387b5adee5d49ffde1d71e89762d97.png'>
										<img class="car-maker tooltipped" data-position="top" data-delay="10" data-tooltip="Audi" src='images/car-logos/11e1627e09b2ad999e782f4cdaa095d7.png'>
										<img class="car-maker tooltipped" data-position="top" data-delay="10" data-tooltip="Audi" src='images/car-logos/21ae4d71caba283773644c83b16b2b8e.png'>
									</div>
										<p class="address">Address, Address, Address </p>
  									<a href="#!" class="secondary-content">
											<i class="tiny material-icons">gradegradegradegradegrade</i><br>
												<p>24 km away</p>
										</a>
  								</li>
									<li class="collection-item avatar garage">
  									<img src="images/amag_profile_logo.png" alt="" class="circle">
  									<h5 class="title">Bosch garage services</h5>
										<div>
										<img class="car-maker tooltipped" data-position="top" data-delay="10" data-tooltip="Audi" src='images/car-logos/f7387b5adee5d49ffde1d71e89762d97.png'>
										<img class="car-maker tooltipped" data-position="top" data-delay="10" data-tooltip="Audi" src='images/car-logos/11e1627e09b2ad999e782f4cdaa095d7.png'>
										<img class="car-maker tooltipped" data-position="top" data-delay="10" data-tooltip="Audi" src='images/car-logos/21ae4d71caba283773644c83b16b2b8e.png'>
									</div>
										<p class="address">Address, Address, Address </p>
  									<a href="#!" class="secondary-content">
											<i class="tiny material-icons">gradegradegradegradegrade</i><br>
												<p>24 km away</p>
										</a>
  								</li>
									<li class="collection-item avatar garage">
  									<img src="images/amag_profile_logo.png" alt="" class="circle">
  									<h5 class="title">Bosch garage services</h5>
										<div>
										<img class="car-maker tooltipped" data-position="top" data-delay="10" data-tooltip="Audi" src='images/car-logos/f7387b5adee5d49ffde1d71e89762d97.png'>
										<img class="car-maker tooltipped" data-position="top" data-delay="10" data-tooltip="Audi" src='images/car-logos/11e1627e09b2ad999e782f4cdaa095d7.png'>
										<img class="car-maker tooltipped" data-position="top" data-delay="10" data-tooltip="Audi" src='images/car-logos/21ae4d71caba283773644c83b16b2b8e.png'>
									</div>
										<p class="address">Address, Address, Address </p>
  									<a href="#!" class="secondary-content">
											<i class="tiny material-icons">gradegradegradegradegrade</i><br>
												<p>24 km away</p>
										</a>
  								</li>
									<li class="collection-item avatar garage">
  									<img src="images/amag_profile_logo.png" alt="" class="circle">
  									<h5 class="title">Bosch garage services</h5>
										<div>
										<img class="car-maker tooltipped" data-position="top" data-delay="10" data-tooltip="Audi" src='images/car-logos/f7387b5adee5d49ffde1d71e89762d97.png'>
										<img class="car-maker tooltipped" data-position="top" data-delay="10" data-tooltip="Audi" src='images/car-logos/11e1627e09b2ad999e782f4cdaa095d7.png'>
										<img class="car-maker tooltipped" data-position="top" data-delay="10" data-tooltip="Audi" src='images/car-logos/21ae4d71caba283773644c83b16b2b8e.png'>
									</div>
										<p class="address">Address, Address, Address </p>
  									<a href="#!" class="secondary-content">
											<i class="tiny material-icons">gradegradegradegradegrade</i><br>
												<p>24 km away</p>
										</a>
  								</li>

  							</ul>
								<ul class="pagination center col s12">
									<li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
									<li class="active"><a href="#!">1</a></li>
									<li class="waves-effect"><a href="#!">2</a></li>
									<li class="waves-effect"><a href="#!">3</a></li>
									<li class="waves-effect"><a href="#!">4</a></li>
									<li class="waves-effect"><a href="#!">5</a></li>
									<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
								</ul>
								<p class="col s12 garage-list-toggle col center">Pick for me</p>
  						</div>
  					</div>
          </div>
					<!-- End of Service form step three - show results -->
					<div class="waves-effect card-action white-text service-form-btn" id="service-form-btn"><a></a></div>
				</form>
				<!-- End of Service form -->
			</div>
		</div>
		<!-- End of Service options and form -->
		<!-- Section 3 features -->
		<div class="container row">
			<div class="col s12 m4">
				<h1 class="center icon-tools-2"></h1>
				<h5 class="center light">Brauchst du einen Service?</h5>
				<p class="center light">Du bist nur 3 Schritte davon entfernt. Du brauchst lediglich deinen Fahrzeugausweis und los gehts.</p>
			</div>
			<div class="col s12 m4">
				<h1 class="center icon-wallet"></h1>
				<h5 class="center light">Willst du Preise vergleichen?</h5>
				<p class="center light">Die Kosten für Garagenbesuche sind oft nicht verständlich. Vergleiche und schaffe Klarheit.</p>
			</div>
			<div class="col s12 m4">
				<h1 class="center icon-clock"></h1>
				<h5 class="center light">Ausserhalb der Öffnungszeiten?</h5>
				<p class="center light">Bei uns kannst du jederzeit Anfragen starten. Egal wo, wann und wie. Jetzt testen!</p>
			</div>
		</div>
		<div class="divider"></div>
		<!-- Section 1 feature -->
		<div class="container section row">
			<h6 class="icon-gears" style="font-size: 36px"></h6>
			<h5 class="light">Wie funktioniert Carhelper?</h5>
			<div class="col s3 divider no-margin-bottom no-margin-top"></div>
			<p class="light full-text">Du brauchst einen Service für dein Auto? Carhelper verschafft dir 3 vergleichbare Offerten für Service und zusätzliche Reparaturen. Egal ob du einen VW Golf oder einen Porsche Cayenne fährst - wir verschaffen dir Transparenz indem wir bei den Garagen deiner Wahl nach den Servicepreisen fragen. Die offerierten Kosten für den Service stellen wir dir in einer leicht verständlichen Übersicht zu und du entscheidest, welche Garage deinen Auftrag erhält. Vielleicht wählst du die Garage, welche dir am nächsten ist, oder die mit einer offiziellen Markenvertretung oder du richtest dich ganz einfach nach dem günstigsten Preis.</p>
			<div class="col s12 divider no-margin-bottom"></div>
		</div>
		<!-- Section 1 feature -->
		<div class="container row">
			<h6 class="icon-wallet" style="font-size: 36px"></h6>
			<h5 class="light">Was kostet mich das?</h5>
			<div class="col s3 divider no-margin-bottom no-margin-top"></div>
			<p class="light full-text">Carhelper ist für dich als Autohalter völlig kostenlos. Unser Service soll dir Transparenz und Vertrauen verschaffen - ein Gut was unserer Meinung nach viel Wert ist, aber nichts kosten sollte. Natürlich müssen aber auch unsere hart arbeitenden Eichhörnchen zu ihren Nüssen kommen. Daher belasten wir den Garagen bei einer erfolgreichen Vermittlung eines neuen Kunden, eine kleine Vermittlungsgebühr.</p>
			<div class="col s12 divider no-margin-bottom"></div>
		</div>
		<!-- Section 1 feature -->
		<div class="container row">
			<h6 class="icon-caution" style="font-size: 36px"></h6>
			<h5 class="light">Was mache ich, wenn die Rechnung der Garage trotzdem höher ist, als die offerierten Kosten?</h5>
			<div class="col s3 divider no-margin-bottom no-margin-top"></div>
			<p class="light full-text">Rechne damit, dass das Nachfüllen von verschiedenen Flüssigkeiten (Hydrauliköl, Scheibenwischwasser, Frostschutz, usw.) 20 - 30 Franken ausmachen kann. Manchmal ist bei deinem Auto auch etwas mehr Liebe und Fürsorge nötig, sodass es mit einem einfachen Service noch nicht getan ist. Gelegentlich müssen beispielsweise Bremsbeläge ersetzt oder die Bremsflüssigkeit ausgetauscht werden. Gut, wenn dich der Mechaniker darauf aufmerksam macht und nicht mit mangelhaften Bremsen nach Hause schickt. Wenn zusätzliche Kosten anfallen, müssen diese aber immer vorher kommuniziert werden - am besten schriftlich. Falls die Rechnung aus dir unbekannten Gründen höher ausfallen sollte, setze dich umgehend mit der Garage in Verbindung. Falls keine Einigkeit zu Stande kommt, kannst du dich gerne an uns wenden. Wir werden deinen Fall prüfen und zwischen dir und der Garage vermitteln. Unser oberstes Ziel ist, dass du durch Carhelper ein positives Erlebnis mit der Garage deiner Wahl erlebst.</p>
		</div>
		<!-- Section video -->
		<div class="parallax-container how-video" class="video-how-it-works-modal-trigger">
			<div class="parallax">
				<img src="images/user_profile_poster.jpg" alt="">
				</div>
				<a href="#video-how-it-works-modal" class="video-how-it-works-modal-trigger">
					<h6 class="col s12 center icon-video" style="font-size: 60px" style="z-index: 999999"></h6>
					<h5 class="center white-text thin">How carhleper works?</h5>
				</a>
		</div>

<?php include 'partners.php';?>

<div class="divider"></div>
		<!-- Section 2 features -->
		<div class="container row feature">
			<div class="col s12 m6 no-padding center feature-item">
				<h6 class="icon-happy" style="font-size: 60px"></h6>
				<h4 class="col center s12 thin no-padding">Over 6000 garages</h4>
				<a href="garages.html"><div class="col s6 offset-s3 btn waves-effect waves-light light btn-line">See all garages</div></a>
			</div>
			<div class="col s12 m6 divider hide-on-med-and-up"></div>
			<div class="col s12 m6 no-padding center feature-item">
				<h6 class="icon-video" style="font-size: 60px"></h6>
				<h4 class="col center s12 thin no-padding">Why garages use it?</h4>
				<a class="video-how-it-works-modal-trigger" href="#video-how-it-works-modal"><div class="col s6 btn offset-s3 waves-effect waves-light light btn-line">Watch video</div></a>
			</div>
		</div>
		<div class="divider"></div>
		<!-- Section Contact -->
		<div class="container row" id="feedback-form-wrapper">
			<h5 class="center col s12 thin no-padding">Was können wir verbessern? Wir freuen uns auf dein Feedback.</h5>
			<div class="col s12  no-padding">
				<div class="col s4 offset-s4 divider blue-grey"></div>
				<p class="col s12 light center  no-padding">Dieser Service ist neu und in ständiger Weiterentwicklung. Wir bitten um Verständnis, falls sich kleine Unannehmlichkeiten einschleichen. Falls du Anregungen oder Fragen hast, teile uns deine Gedanken mit.</p>
				<form id="feedback-form" class="col s12">
					<div class="row">
						<div class="input-field col s12">
							<input id="email" type="email" class="validate">
							<label for="email">Email</label>
						</div>
						<div class="input-field col s12">
							<textarea id="textarea1" class="materialize-textarea"></textarea>
							<label for="textarea1">Sag uns deine Meinung...</label>
						</div>
					</div>
				</form>
				<div class="col s12">
					<div id="feedback-btn"class=" col s6 offset-s3 btn waves-effect waves-light light btn-line">Feedback abgeben</div>
				</div>
			</div>
		</div>
		<div class="divider"></div>
		<!-- Section Feedback -->
		<div class="container row">
			<div class="col s12">
				<h6 class="icon-megaphone center" style="font-size: 48px"></h6>
				<h4 class="col center s12 thin">Die Eindrücke unserer Kunden.</h4>
			</div>
			<div class="col s12 m8 offset-m2 l6">
				<div class="card-panel grey lighten-5 z-depth-1">
					<div class="row valign-wrapper">
						<div class="col s2">
							<img src="images/silvan_new.png" alt="" class="circle responsive-img">
						</div>
						<div class="col s10">
							<span>
							Perfekt für alle die schon schlechte Erfahrungen mit Garagen gemacht haben. Der Service ist kostenlos und total easy, da man nur das Fahrzeugpapier hochladen muss.<br><b>Larissa H.</b>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col s12 m8 offset-m2 l6">
				<div class="card-panel grey lighten-5 z-depth-1">
					<div class="row valign-wrapper">
						<div class="col s2">
							<img src="images/silvan_new.png" alt="" class="circle responsive-img"> <!-- notice the "circle" class -->
						</div>
						<div class="col s10">
							<span>
							PMir wurde carhelper von einem Freund empfohlen. Da mein Auto sowieso wiedermal in den Service musste, hab ichs schnell ausprobiert. Es war sehr einfach und ich habe...<br><b>Silvan B.</b>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Vudeo background -->
		<div class="background-overlay"></div>
		<video autoplay="" class="background-video" loop="" class="fillWidth fadeIn animated" poster="video/background_video.gif">
			<source src="video/background_video.mp4" type="video/mp4">
			Your browser does not support the video tag. I suggest you upgrade your browser.
		</video>

<!-- Video modal -->
<div class="row">
   <div id="video-how-it-works-modal" class="modal col s10 offset-s1">
     <div class="modal-content">
			 <iframe width="100%" height="500px" src="https://www.youtube.com/embed/DekL5q5e9lM" frameborder="0" allowfullscreen></iframe>
		 </div>
 </div>
</div>


<?php include 'footer.php';?>
