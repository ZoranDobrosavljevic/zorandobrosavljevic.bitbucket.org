<div class="messanger">
  <p>Messages:</p>
  <div class="messages-list">
    <div class="message">
      <div class="message-bubble">
        <img src="images/apple-touch-icon-152x152.png" alt="Contact Person">
        <p class="sender">Amag</p>
        <p class="body">Lorem ipsum dolor sit amet, consectetur adipisicing</p>
        <p class="time">26. June 2016, 14:00</p>
      </div>
    </div>
    <div class="message">
      <div class="message-bubble-right">
        <img src="images/Aleksandar_Stevanovic_Carhelper.ch_auto_service.jpg" alt="Contact Person">
        <p class="sender">Alex</p>
        <p class="body">Lorem ipsum dolor sit amet, consectetur</p>
        <p class="time">14:00</p>
      </div>
    </div>
    <div class="message">
      <div class="message-bubble">
        <img src="images/amag_profile_logo.png" alt="Contact Person">
        <p class="sender">Amag</p>
        <p class="body">Lorem ipsu</p>
        <p class="time">14:00</p>
      </div>
    </div>
    <div class="message">
      <div class="message-bubble-right">
        <img src="images/Aleksandar_Stevanovic_Carhelper.ch_auto_service.jpg" alt="Contact Person">
        <p class="sender">Alex</p>
        <p class="body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, Lorem ipsum dolort amet, consectetur adipisicing elit, </p>
        <p class="time">14:00</p>
      </div>
    </div>
    <div class="message">
      <div class="message-bubble">
        <img src="images/amag_profile_logo.png" alt="Contact Person">
        <p class="sender">Amag</p>
        <p class="body">Lorem ipsum dolor sit</p>
        <p class="time">14:00</p>
      </div>
    </div>
    <div class="message">
      <div class="message-bubble-right">
        <img src="images/Aleksandar_Stevanovic_Carhelper.ch_auto_service.jpg" alt="Contact Person">
        <p class="sender">Alex</p>
        <p class="body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, </p>
        <p class="time">14:00</p>
      </div>
    </div>
  </div>
  <div class="messanger-input">
    <input type="text"><a href="">Send</a>
  </div>
</div>
