<?php include 'header.php';?>

<!-- Parallax background -->
<!-- <div class="parallax-container user-profile">
  <div class="parallax"><img src="images/user_profile_poster.jpg" alt=""></div>
</div> -->



<div class="container row user-offers user-profile dashboard">
<h5 class="col s12 light">Offer for Easy service No.12234:</h5>
<div class="col m4 s12">
  <ul class="collection  z-depth-1 with-header hoverable ">
      <li class="collection-item avatar">
        <img src="images/amag_profile_logo.png" alt="" class="circle">
        <span class="title">Amag</span>
        <p>First Line <br>Second Line</p>
        <a href="#!" class="secondary-content"><i class="tiny material-icons">gradegradegradegradegrade</i></a>
      </li>
      <li class="collection-item"><i class="cyan-text text-accent-4 material-icons">label_outline</i> 1.000 CHF</li>
      <li class="collection-item"><i class="cyan-text text-accent-4 material-icons">navigation</i> 10 km  away</li>
      <li class="collection-item"><i class="cyan-text text-accent-4 material-icons">perm_contact_calendar</i> 12. 04 2016.</li>
      <li class="collection-item light-green lighten-2"><i class="white-text material-icons">done</i> Oil change</li>
      <li class="collection-item "><i class="cyan-text text-accent-4 material-icons">done</i> Oil change</li>
      <li class="collection-item "><i class="cyan-text text-accent-4 material-icons">done</i> Oil change</li>
      <li class="collection-item "><i class="cyan-text text-accent-4 material-icons">done</i> Oil change</li>
      <li class="collection-item "><i class="cyan-text text-accent-4 material-icons">done</i> Oil change</li>
      <li class="btn btn-large waves-effect waves-light col s12">BOCHEN</li>
    </ul>
</div>
  <div class="col m4 s12">
    <ul class="collection  z-depth-1 with-header hoverable ">
        <li class="collection-item avatar">
          <img src="images/amag_profile_logo.png" alt="" class="circle">
          <span class="title">Amag</span>
          <p>First Line <br>Second Line</p>
          <a href="#!" class="secondary-content"><i class="tiny material-icons">gradegradegradegradegrade</i></a>
        </li>
        <li class="collection-item"><i class="cyan-text text-accent-4 material-icons">label_outline</i> 1.000 CHF</li>
        <li class="collection-item"><i class="cyan-text text-accent-4 material-icons">navigation</i> 10 km  away</li>
        <li class="collection-item"><i class="cyan-text text-accent-4 material-icons">perm_contact_calendar</i> 12. 04 2016.</li>
        <li class="collection-item light-green lighten-2"><i class="white-text material-icons">done</i> Oil change</li>
        <li class="collection-item "><i class="cyan-text text-accent-4 material-icons">done</i> Oil change</li>
        <li class="collection-item "><i class="cyan-text text-accent-4 material-icons">done</i> Oil change</li>
        <li class="collection-item "><i class="cyan-text text-accent-4 material-icons">done</i> Oil change</li>
        <li class="collection-item "><i class="cyan-text text-accent-4 material-icons">done</i> Oil change</li>
        <li class="btn btn-large waves-effect waves-light col s12">BOCHEN</li>
      </ul>
  </div>
  <div class="col m4 s12">
    <ul class="collection  z-depth-1 with-header hoverable ">
        <li class="collection-item avatar">
          <img src="images/amag_profile_logo.png" alt="" class="circle">
          <span class="title">Amag</span>
          <p>First Line <br>Second Line</p>
          <a href="#!" class="secondary-content"><i class="tiny material-icons">gradegradegradegradegrade</i></a>
        </li>
        <li class="collection-item"><i class="cyan-text text-accent-4 material-icons">label_outline</i> 1.000 CHF</li>
        <li class="collection-item"><i class="cyan-text text-accent-4 material-icons">navigation</i> 10 km  away</li>
        <li class="collection-item"><i class="cyan-text text-accent-4 material-icons">perm_contact_calendar</i> 12. 04 2016.</li>
        <li class="collection-item light-green lighten-2"><i class="white-text material-icons">done</i> Oil change</li>
        <li class="collection-item "><i class="cyan-text text-accent-4 material-icons">done</i> Oil change</li>
        <li class="collection-item "><i class="cyan-text text-accent-4 material-icons">done</i> Oil change</li>
        <li class="collection-item "><i class="cyan-text text-accent-4 material-icons">done</i> Oil change</li>
        <li class="collection-item "><i class="cyan-text text-accent-4 material-icons">done</i> Oil change</li>
        <li class="btn btn-large waves-effect waves-light col s12">BOCHEN</li>
      </ul>
  </div>
</div>


<div class="divider"></div>
<!-- Section Partners -->
<div class="container partners-container">
  <h5 class="light">Charhelper trusted partners</h5>
  <div class="partners row">
    <i class="col s3 m2 icon-amag"></i>
    <i class="col s3 m2 icon-bosch"></i>
    <i class="col s3 m2 icon-amag"></i>
    <i class="col s3 m2 icon-garage-plus"></i>
    <i class="col s6 m2 icon-amag"></i>
    <i class="col s6 m2 icon-garage-plus"></i>
  </div>
</div>

<?php include 'footer.php';?>
