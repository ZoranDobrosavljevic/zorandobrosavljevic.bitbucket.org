<?php include 'header.php';?>

<!-- Parallax background -->
<!-- <div class="parallax-container user-profile">
  <div class="parallax"><img src="images/user_profile_poster.jpg" alt=""></div>
</div> -->





<div class="container garage-feedbacks">
    <div class="row">
            <div class="col s12 m6">
              <div class="collection z-depth-1">
                <div class="collection-item">
                  <h5>Zusatzaufwand</h5>
                </div>
                <div class="collection-item">
                  <i class="cyan-text text-accent-4 material-icons">done</i> bremsbeläge hinten
                </div>
                <h5 class="collection-item">Kosten: CHF 390.00</h5>
                <div class="collection-item">
                  <h5>Ausführen?</h5>
                  <div class="row">
                    <div class=" col s5 waves-effect btn btn-large light-green darken-2">Ja</div>
                    <div class="col s5 waves-effect right btn btn-large deep-orange darken-3">Nein</div>
                  </div>
                </div>
                <ul class="collection-item collapsible" data-collapsible="accordion">
                    <li>
                      <div class="collapsible-header"><i class="material-icons">info_outline</i>more</div>
                      <div class="collapsible-body">
                        <p>Lorem ipsum dolor sit amet.</p>
                        <div class="section row">
                          <img src="images/user_profile_pic.jpg" alt="" class=" col s2 responsive-img">
                          <img src="images/user_profile_pic.jpg" alt="" class=" col s2 responsive-img">
                          <img src="images/user_profile_pic.jpg" alt="" class=" col s2 responsive-img">
                          <img src="images/user_profile_pic.jpg" alt="" class=" col s2 responsive-img">
                          <img src="images/user_profile_pic.jpg" alt="" class=" col s2 responsive-img">
                          <img src="images/user_profile_pic.jpg" alt="" class=" col s2 responsive-img">
                        </div>
                      </div>
                    </li>
                  </ul>

              </div>
        </div>

        <div class="col s12 m6 messages-wrapper card">
          <h6 style="padding: 10px 20px">Convesation:</h6>
          <div class="divider"></div>
					<div class="messages-list">
          <div class="feedback row">
            <div class="user col s4 m2">
              <img class="profile-pic col s12" src="images/user_profile_pic.jpg"/>
              <span class="name col s12">Alex</span>
            </div>
            <div class="feedback-content col s12 m10">
              <p class="text">Message from Alex Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
              <p class="date">Yesterday, 14:00</p>
            </div>
            <div class="divider col s12 "></div>
          </div>
          <div class="feedback row">
            <div class="user col s4 m2 right">
              <img class="profile-pic col s12" src="images/amag_profile_logo.png"/>
              <span class="name col s12">Amag</span>
            </div>
            <div class="feedback-content col m8 offset-m2 s12">
              <p class="text">Message from you to Alex Message from you to Alex Message from you to Alex Message from you to Alex</span>
              <p class="date">26. June 2016, 14:00</p>
            </div>

            <div class="divider col s12"></div>
          </div>
					<div class="feedback row">
						<div class="user col s4 m2">
							<img class="profile-pic col s12" src="images/user_profile_pic.jpg"/>
							<span class="name col s12">Alex</span>
						</div>
						<div class="feedback-content col s12 m10">
							<p class="text">Message from Alex Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
							<p class="date">Yesterday, 14:00</p>
						</div>
						<div class="divider col s12 "></div>
					</div>
					<div class="feedback row">
						<div class="user col s4 m2 right">
							<img class="profile-pic col s12" src="images/amag_profile_logo.png"/>
							<span class="name col s12">Amag</span>
						</div>
						<div class="feedback-content col m8 offset-m2 s12">
							<p class="text">Message from you to Alex Message from you to Alex Message from you to Alex Message from you to Alex</span>
							<p class="date">26. June 2016, 14:00</p>
						</div>

						<div class="divider col s12"></div>
					</div>

				</div>
          <div class="row">
            <div class="input-field col s12">
              <input class="col s10" id="comment" type="text" placeholder="Message for Alex">
              <!-- <label for="comment">Message for Alex</label> -->
            <a href="#!" class="col 2 secondary-content"><i class="material-icons">send</i></a>
            </div>
          </div>

        </div>

</div>
</div>



















<?php include 'footer.php';?>
