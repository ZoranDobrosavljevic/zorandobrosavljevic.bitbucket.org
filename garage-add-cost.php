<?php include 'header.php';?>

<div class="container row">
  <div class="col s12">
      <h5 class="col s12"><span class="deep-orange-text icon-official-service_small-11 icon"> </span> Easy service No. 12345</h5>
      <span class="col s12de">BMW series 3, 2004 <br>Owner: Alex</span>
  </div>
  <div class="col s12 m6">
    <div class="col s12">
      <ul class="collection  z-depth-1 hoverable ">
          <li class="collection-item"><i class="cyan-text text-accent-4 material-icons">label_outline</i> Price 1.000 CHF</li>
          <li class="collection-item"><i class="cyan-text text-accent-4 material-icons">perm_contact_calendar</i> Date 12. 04 2016.</li>
          <li class="collection-item"><i class="cyan-text text-accent-4 material-icons">done</i> Oil changes</li>
          <li class="collection-item"><i class="cyan-text text-accent-4 material-icons">done</i> Washing inside</li>
          <li class="collection-item"><i class="cyan-text text-accent-4 material-icons">done</i> Car replament</li>
          <li class="grey-text collection-item"><i class="grey-text material-icons">done</i> Oil changes</li>
        </ul>
        <div id="addExtraWorksBtn" class="col s12 right waves-effect btn" >Add extra works</div>
    </div>
    <div id="extra-works" class="col s12">
      <div class="card">
        <div class="card-content row">
          <h4 class="col s12 card-title">Additional works:</h4>
          <div class="col s12 m6">
            <p><input name="group1" type="radio" id="test1" class="with-gep"/><label for="test1">Necessary works</label></p>
             <p><input name="group1" type="radio" id="test2" /><label for="test2">Good to be fixed</label></p>
             <p><input name="group1" type="radio" id="test3" /><label for="test3">Optional works</label></p>
         </div>
         <div class="section col s12 m6">
          <p><input type="checkbox" id="check1"/><label for="check1">Air filter change</label></p>
          <p><input type="checkbox" id="check2"/><label for="check2">Air filter change</label></p>
          <p><input type="checkbox" id="check3"/><label for="check3">Air filter change</label>
          </p>
        </div>
        <br>
        <div class="section col s12 file-field input-field">
          <div class="btn">
            <span>Upload pictures</span>
            <input type="file" multiple>
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" placeholder="Upload one or more files">
          </div>
        </div>
          <div class="col s12 input-field">
            <input id="comment" type="text">
            <label for="comment">Note</label>
          </div>

          <div class="col s12 input-field">
            <input id="extra-cost" type="number">
            <label for="extra-cost">Enter extra cost (CHF)</label>
          </div>
        </div>
        <div class="card-action right-align">
          <a href="#">Send to Alex</a>
        </div>
      </div>

    </div>
  </div>


<div class="col s12 m6 garage-feedbacks">
  <div class="feedback row">
    <div class="user col s4 m2">
      <img class="profile-pic col s12" src="images/user_profile_pic.jpg"/>
      <span class="name col s12">Alex</span>
    </div>
    <div class="feedback-content col s12 m10">
      <p class="text">Message from Alex Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
      <p class="date">Yesterday, 14:00</p>
    </div>
    <div class="divider col s12 "></div>
  </div>
  <div class="feedback row">
    <div class="user col s4 m2 right">
      <img class="profile-pic col s12" src="images/amag_profile_logo.png"/>
      <span class="name col s12">Amag</span>
    </div>
    <div class="feedback-content col m8 offset-m2 s12">
      <p class="text">Message from you to Alex Message from you to Alex Message from you to Alex Message from you to Alex</span>
      <p class="date">26. June 2016, 14:00</p>
    </div>

    <div class="divider col s12"></div>
  </div>

  <div class="row">
    <div class="input-field col s12">
      <input class="col s10" id="comment" type="text">
      <label for="comment">Message for Alex</label>
    <a href="#!" class="col 2 secondary-content"><i class="material-icons">send</i></a>
    </div>
  </div>

</div>
</div>

<?php include 'footer.php';?>
