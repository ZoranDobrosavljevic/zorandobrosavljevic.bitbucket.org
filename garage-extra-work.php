<?php include 'header.php';?>

<div class="container service-dashboard">
	<div class="row">
	<div class="col s12 m6 service-details">
		<h5 class="col s12">Easy service for BMW 320d 2004</h5>
		<ul class="collection">
			<li class="collection-item"><i class="material-icons"></i>Your offer:<span class="badge primary-color ">1.550,00 CHF</span></li>
			<li class="collection-item"><i class="material-icons"></i>Date:<span class="badge">21. June 2016.</span></li>
			<li class="collection-item"><i class="material-icons"></i>Owner:<span class="badge">Alex</span></li>
		</ul>
		<p>Need to be done:</p>
		<ul class="collection">
	       <li class="collection-item"><i class="material-icons">done</i> Oil change</li>
	       <li class="collection-item"><i class="material-icons">done</i> Oil change</li>
				 <li class="collection-item"><i class="material-icons">done</i> Oil change</li>
				 <li class="collection-item"><i class="material-icons">done</i> Oil change</li>
				 <li class="collection-item"><i class="material-icons">done</i> Oil change</li>
				 <li class="collection-item"><i class="material-icons">done</i> Oil change</li>
	  </ul>
		<div class="no-padding col s12">
			<a class="btn add-extra-works-trigger right" href="#add-extra-works"><span>Add extra works</span></a>
		</div>
	</div>

	<?php include 'messanger.php';?>

	</div>
</div>




<div id="floating-contant-btn" class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
	<a href="#feedback-form-wrapper"class="btn-floating btn-large">
  	<i class="fa fa-envelope-o" aria-hidden="true"></i>
  </a>
</div>


<!-- Add extra works -->
<div class="row">
   <div id="add-extra-works" class="modal s12">
    <div class="col s12 modal-content">
     <h4>Add extra works</h4>
    <p>Easy service for BMW 320d 2004</p>
		</div>
		<div class="col s12 divider"></div>
		<div class="col s12 modal-content">
		<div class="col s12 m6">
			<p><input name="group1" type="radio" id="test1" class="with-gep"/><label for="test1">Necessary works</label></p>
			<p><input name="group1" type="radio" id="test2" /><label for="test2">Good to be fixed</label></p>
			<p><input name="group1" type="radio" id="test3" /><label for="test3">Optional works</label></p>
		 </div>
		 <div class="section col s12 m6">
			<p><input type="checkbox" id="check1"/><label for="check1">Air filter change</label></p>
			<p><input type="checkbox" id="check2"/><label for="check2">Air filter change</label></p>
			<p><input type="checkbox" id="check3"/><label for="check3">Air filter change</label>
			</p>
		</div>
		<br>
		<div class="section col s12 file-field input-field">
			<div class="btn">
				<span>Upload pictures</span>
				<input type="file" multiple>
			</div>
			<div class="file-path-wrapper">
				<input class="file-path validate" type="text" placeholder="Upload one or more files">
			</div>
		</div>
			<div class="col s12 input-field">
				<input id="comment" type="text">
				<label for="comment">Note</label>
			</div>

			<div class="col s12 input-field">
				<input id="extra-cost" type="number">
				<label for="extra-cost">Enter extra cost (CHF)</label>
			</div>
			<div class="btn col s6 right">Add extra works</div>
		 </div>
   </div>
</div>

<div id="floating-contant-btn" class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
	<a href="#feedback-form-wrapper"class="btn-floating btn-large">
  	<i class="fa fa-envelope-o" aria-hidden="true"></i>
  </a>
</div>

<?php include 'footer.php';?>
