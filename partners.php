<div class="container partners-container">
  <h5 class="light">Charhelper trusted partners</h5>
  <div class="partners row">
    <i class="col s3 m2 icon-amag"></i>
    <i class="col s3 m2 icon-bosch"></i>
    <i class="col s3 m2 icon-amag"></i>
    <i class="col s3 m2 icon-garage-plus"></i>
    <i class="col s6 m2 icon-amag"></i>
    <i class="col s6 m2 icon-garage-plus"></i>
  </div>
</div>
