<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		<meta name="format-detection" content="telephone=no">
		<meta name="theme-color" content="#ff5722">
		<title>Carhelper</title>
		<link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon-152x152.png">
		<link rel="icon" href="images/favicon-32x32.png" sizes="32x32">
		<link href="css/carhelper.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	</head>
	<body>
		<div class="navbar">
			<nav id="nav-transparent" role="navigation" class="nav-transparent">
				<div class="nav-wrapper container">
					<a id="logo-container" href="index.php" class="brand-logo">
					<span class="icon-logo"></span>
					</a>
					<ul class="right hide-on-med-and-down">
						<li><a href="about-us.php">About us</a></li>
						<li><a href="garages.php">Garages</a></li>
						<li><a class="dropdown-button" href="#!" data-activates="garage-links">Garage links<i class="material-icons right">arrow_drop_down</i></a></li>
						<li><a class="dropdown-button" href="#!" data-activates="user-links">User links<i class="material-icons right">arrow_drop_down</i></a></li>
						<li><a class="login-modal-trigger" href="#login-modal">Login</a></li>
		        <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
					</ul>
					<ul id="nav-mobile" class="side-nav">
						<li><a href="garage-profile.php">Garage profile</a></li>
						<li><a href="garage-all-services.php">All services</a></li>
						<li><a href="garage-new-service.php">New service</a></li>
						<li><a href="garage-extra-work.php">Add works</a></li>
						<li><div class="divider"></div></li>
						<li><a href="user-new-offers.php">New offer</a></li>
						<li><a href="user-extra-work.php">Extra works</a></li>
						<li><div class="divider"></div></li>
						<li><a href="about-us.php">About us</a></li>
						<li><a href="garages.php">Garages</a></li>
						<li><a class="login-modal-trigger" href="#login-modal">Login</a></li>
						<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
					</ul>
					<ul id="garage-links" class="dropdown-content">
					  <li><a href="garage-profile.php">Garage profile</a></li>
					  <li><a href="garage-all-services.php">All services</a></li>
					  <li><a href="garage-new-service.php">New service</a></li>
						<li><a href="garage-extra-work.php">Add works</a></li>
					</ul>
					<ul id="user-links" class="dropdown-content">
						<li><a href="user-new-offers.php">New offer</a></li>
					  <li><a href="user-extra-work.php">Extra works</a></li>
					</ul>
					<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="icon-menu"></i></a>
				</div>
			</nav>
		</div>
