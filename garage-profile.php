<?php include 'header.php';?>

  <div class="parallax-container garage-profile">
    <div class="parallax"><img src="images/user_profile_poster.jpg" alt=""></div>

  </div>

<div class="container garage-info white  " >
		<div class="row z-depth-1 card">
      <img class="col s3 m2 l1 circle responsive-img garage-logo" src="images/amag_profile_logo.png"></img>
      <div class="col s9 m5 l7">
        <h5 class="name">Phoenix Garage Beat Perucchi</h5>
        <p class="details"><i class="tiny material-icons deep-orange-text">phone</i> Tösstalstrasse 111</p>
        <p class="details"><i class="tiny material-icons deep-orange-text">phone</i> 011 / 111 - 555</p>
        <p class="details"><i class="tiny material-icons deep-orange-text">email</i> email@email.com</p>
        <p class="details"><i class="tiny material-icons deep-orange-text">phone</i> www.phoenix-garage.ch</p>
      </div>
      <div class="col s12 m5 l4 working-hours">
        <span class="col s12 title">Working hours:</span>
        <div class="divider col s12"></div>
        <span class="col s4">Monday</span><span class="col s4">8-12</span><span class="col s4">13-17</span>
        <span class="col s4">Tuesday</span><span class="col s4">8-12</span><span class="col s4">13-17</span>
        <span class="col s4">Wednesday</span><span class="col s4">8-12</span><span class="col s4">13-17</span>
        <span class="col s4">Thursday</span><span class="col s4">8-12</span><span class="col s4">13-17</span>
        <span class="col s4">Friday</span><span class="col s4">8-12</span><span class="col s4">13-17</span>
        <span class="col s4">Saturday</span><span class="col s4">7-13</span>
      </div>
      <div class="divider col s12"></div>
      <div class="col s12 brands">
        <h6 class="thin blue-grey-text darken-3">Official service for</h6>
        <img class="col" src="images/car_logo/car_logo_PNG1653.png"></img>
        <img class="col " src="images/car_logo/car_logo_PNG1669.png"></img>
        <img class="col" src="images/car_logo/car_logo_PNG1642.png"></img>
      </div>
		</div>
  </div>

  <!-- <div class="garage-map">
      <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
    <div style='overflow:hidden;height:440px;width:100%;'>
      <div id='gmap_canvas' style='height:440px;width:100%;'></div>
      <style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
    </div>
    <script type='text/javascript'>function init_map(){var myOptions = {zoom:10,center:new google.maps.LatLng(51.5073509,-0.12775829999998223),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(51.5073509,-0.12775829999998223)});infowindow = new google.maps.InfoWindow({content:'<strong>Title</strong><br>London, United Kingdom<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);
    </script>
    <div class="overlay"></div>
  </div> -->

	<div class="container">
	<div class="row">
  	<div class="col s12 m6">
      <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
    <div style='overflow:hidden;height:350px;width:100%;' class="card  garage-map-inline">
      <div id='gmap_canvas' style='height:350px;width:100%;'></div>
      <style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
    </div>
    <script type='text/javascript'>function init_map(){var myOptions = {zoom:10,center:new google.maps.LatLng(51.5073509,-0.12775829999998223),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(51.5073509,-0.12775829999998223)});infowindow = new google.maps.InfoWindow({content:'<strong>Title</strong><br>London, United Kingdom<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);
    </script>
    <div class="overlay"></div>
  </div>

  <div class="col s12 m6">
		<div class="card garage-about">
      <h4>About us<h4>
      <p>We are official Audi service, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.<p/>
			</div>
	</div>
	</div>
</div>
  <div class="container garage-brands">
  </div>
    <div class="container row z-depth-1 garage-gallery">
      <img class="col s2 no-padding" src="images/amag_profile_logo.png"></img>
      <img class="col s2 no-padding" src="images/amag_profile_logo.png"></img>
      <img class="col s2 no-padding" src="images/amag_profile_logo.png"></img>
      <img class="col s2 no-padding" src="images/amag_profile_logo.png"></img>
      <img class="col s2 no-padding" src="images/amag_profile_logo.png"></img>
      <img class="col s2 no-padding" src="images/amag_profile_logo.png"></img>
    </div>
  </div>

  <div class="container garage-available-services">
  </div>


  <div class="container garage-feedbacks">
    <div class="row">
      <h5 class="col s5 ">64 Reviews <i class="tiny material-icons deep-orange-text">star star star star </i></h5>
      <span class="btn col s3 btn-flat  waves-effect right ">Leave feedback</span>
    </div>

    <div class="feedback row">
      <div class="user col s3">
        <img class="profile-pic col s12" src="images/user_profile_pic.jpg"/>
        <span class="name col s12">Alex</span>
      </div>
      <div class="feedback-content col s9">
        <i class="tiny material-icons deep-orange-text">star star star star </i>
        <p class="text">Perfect and accurate work. 5 ***** for this garage. Every recommendation Lorem ipusm lorem ipsum</span>
        <p class="date">June 2016</p>
      </div>
      <div class="divider col s9 col offset-s3"></div>
    </div>

    <div class="feedback row">
      <div class="user col s3">
        <img class="profile-pic col s12" src="images/user_profile_pic.jpg"/>
        <span class="name col s12">Alex</span>
      </div>
      <div class="feedback-content col s9">
        <i class="tiny material-icons deep-orange-text">star star star star </i>
        <p class="text">Perfect and accurate work. 5 ***** for this garage. Every recommendation Lorem ipusm lorem ipsum</span>
        <p class="date">June 2016</p>
      </div>
      <div class="divider col s9 col offset-s3"></div>
    </div>

    <div class="feedback row">
      <div class="user col s3">
        <img class="profile-pic col s12" src="images/user_profile_pic.jpg"/>
        <span class="name col s12">Alex</span>
      </div>
      <div class="feedback-content col s9">
        <i class="tiny material-icons deep-orange-text">star star star star </i>
        <p class="text">Perfect and accurate work. 5 ***** for this garage. Every recommendation Lorem ipusm lorem ipsum</span>
        <p class="date">June 2016</p>
      </div>
      <div class="divider col s9 col offset-s3"></div>
    </div>

  </div>

  <!-- Section Partners -->
  <div class="container partners-container">
    <h5 class="light">Charhelper trusted partners</h5>
    <div class="partners row">
      <i class="col s3 m2 icon-amag"></i>
      <i class="col s3 m2 icon-bosch"></i>
      <i class="col s3 m2 icon-amag"></i>
      <i class="col s3 m2 icon-garage-plus"></i>
      <i class="col s6 m2 icon-amag"></i>
      <i class="col s6 m2 icon-garage-plus"></i>
    </div>
  </div>

<?php include 'footer.php';?>
