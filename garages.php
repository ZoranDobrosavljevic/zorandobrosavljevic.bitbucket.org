<?php include 'header.php';?>

  <div class="parallax-container user-profile">
    <div class="parallax"><img src="images/user_profile_poster.jpg" alt=""></div>
  </div>


<div class="container">
  <ul class="collection z-depth-1">

    <li class="collection-item">
      <div class="row">
          <form class="col m8 s12">
              <div class="input-field col s12">
                <i class="material-icons prefix">search</i>
                <input id="search" type="text" class="validate">
                <label for="icon_prefix">Search garages</label>
              </div>
          </form>

          <form class="col  col s12 m4">
              <div class="input-field">
                <input id="search" type="text" class="validate">
                <label for="icon_prefix">Set your location</label>
              </div>
          </form>
          <div class="s12 m6 right">
            <div class="left">
              <a style="margin-top: 20px" class='col s12 dropdown-button white btn waves-effect waves-orange btn-flat' href='#' data-activates='dropdown1'>Zürich</a>
             <!-- Dropdown Structure -->
               <ul id='dropdown1' class='dropdown-content'>
                 <li><a href="#!">Luzern</a></li>
                 <li class="divider"></li>
                 <li><a href="#!">Bern</a></li>
                 <li class="divider"></li>
                 <li><a href="#!">Uri</a></li>
               </ul>
             </div>
            <div class="left">
              <a style="margin-top: 20px" class='col s12 dropdown-button white btn waves-effect waves-orange btn-flat' href='#' data-activates='dropdown2'>Nearest first</a>
             <!-- Dropdown Structure -->
               <ul id='dropdown2' class='dropdown-content'>
                 <li><a href="#!">Nearest first</a></li>
                 <li class="divider"></li>
                 <li><a href="#!">Best rated first</a></li>
                 <li class="divider"></li>
                 <li><a href="#!">Order by name</a></li>
               </ul>
             </div>
         </div>


      </div>
    </li>


    <li class="collection-item avatar">
      <img src="images/amag_profile_logo.png" alt="" class="circle">
      <span class="title">Amag</span>
      <p class="grey-text light">
        <i class="tiny material-icons">location_on</i> Address<br>
        <i class="tiny material-icons">phone</i> Second Line<br>
      </p><br>
      <div class="divider divider-5"></div>
      <div class="col s12 brands">
        <p class="left thin blue-grey-text darken-3">Official service for</p>
        <img class="col" src="images/car_logo/car_logo_PNG1643.png"></img>
        <img class="col " src="images/car_logo/car_logo_PNG1645.png"></img>
      </div>
      <div class="secondary-content star-ratings">
        <div class="star-ratings-top" style="width: 60%"><span>★★★★★</span></div>
        <div class="star-ratings-bottom"><span>★★★★★</span></div>
      </div>

      <p class="secondary-content brands">Distance 24 km</>
    </li>


    <li class="collection-item avatar">
      <img src="images/amag_profile_logo.png" alt="" class="circle">
      <span class="title">Amag</span>
      <p class="grey-text light">
        <i class="tiny material-icons">location_on</i> Address<br>
        <i class="tiny material-icons">phone</i> Second Line<br>
      </p><br>
      <div class="divider divider-5"></div>
      <div class="col s12 brands">
        <p class="left thin blue-grey-text darken-3">Official service for</p>
        <img class="col" src="images/car_logo/car_logo_PNG1643.png"></img>
        <img class="col " src="images/car_logo/car_logo_PNG1645.png"></img>
      </div>
      <div class="secondary-content star-ratings">
        <div class="star-ratings-top" style="width: 60%"><span>★★★★★</span></div>
        <div class="star-ratings-bottom"><span>★★★★★</span></div>
      </div>

      <p class="secondary-content brands">Distance 24 km</>
    </li>

    <li class="collection-item avatar">
      <img src="images/amag_profile_logo.png" alt="" class="circle">
      <span class="title">Amag</span>
      <p class="grey-text light">
        <i class="tiny material-icons">location_on</i> Address<br>
        <i class="tiny material-icons">phone</i> Second Line<br>
      </p><br>
      <div class="divider divider-5"></div>
      <div class="col s12 brands">
        <p class="left thin blue-grey-text darken-3">Official service for</p>
        <img class="col" src="images/car_logo/car_logo_PNG1643.png"></img>
        <img class="col " src="images/car_logo/car_logo_PNG1645.png"></img>
      </div>
      <div class="secondary-content star-ratings">
        <div class="star-ratings-top" style="width: 60%"><span>★★★★★</span></div>
        <div class="star-ratings-bottom"><span>★★★★★</span></div>
      </div>
      <p class="secondary-content brands">Distance 24 km</>
    </li>

    <li class="collection-item avatar">
      <img src="images/amag_profile_logo.png" alt="" class="circle">
      <span class="title">Amag</span>
      <p class="grey-text light">
        <i class="tiny material-icons">location_on</i> Address<br>
        <i class="tiny material-icons">phone</i> Second Line<br>
      </p><br>
      <div class="divider divider-5"></div>
      <div class="col s12 brands">
        <p class="left thin blue-grey-text darken-3">Official service for</p>
        <img class="col" src="images/car_logo/car_logo_PNG1643.png"></img>
        <img class="col " src="images/car_logo/car_logo_PNG1645.png"></img>
      </div>
      <div class="secondary-content star-ratings">
        <div class="star-ratings-top" style="width: 60%"><span>★★★★★</span></div>
        <div class="star-ratings-bottom"><span>★★★★★</span></div>
      </div>
      <p class="secondary-content brands">Distance 24 km</>
    </li>
      <li class="collection-item avatar">
        <img src="images/amag_profile_logo.png" alt="" class="circle">
        <span class="title">Amag</span>
        <p>Address<br>Second Line</p>
        <i class="secondary-content material-icons small">gradegradegrade</i>
      </li>
      <li class="collection-item avatar">
        <img src="images/amag_profile_logo.png" alt="" class="circle">
        <span class="title">Amag</span>
        <p>Address<br>Second Line</p>
        <i class="secondary-content material-icons small">gradegradegrade</i>
      </li>

      <li class="collection-item avatar">
        <img src="images/amag_profile_logo.png" alt="" class="circle">
        <span class="title">Amag</span>
        <p class="grey-text light">
          <i class="tiny material-icons">location_on</i> Address<br>
          <i class="tiny material-icons">phone</i> Second Line<br>
        </p><br>
        <div class="divider divider-5"></div>
        <div class="col s12 brands">
          <p class="left thin blue-grey-text darken-3">Official service for</p>
          <img class="col" src="images/car_logo/car_logo_PNG1643.png"></img>
          <img class="col " src="images/car_logo/car_logo_PNG1645.png"></img>
        </div>
        <i class="secondary-content material-icons small">gradegradegradegradegrade</i>
        <p class="secondary-content brands">Distance 24 km</>
      </li>
      <li class="collection-item avatar">
        <img src="images/amag_profile_logo.png" alt="" class="circle">
        <span class="title">Amag</span>
        <p>Address<br>Second Line</p>
        <p class="grey-text secondary-content">No rating</p>
      </li>
      <li class="collection-item avatar">
        <img src="images/amag_profile_logo.png" alt="" class="circle">
        <span class="title">Amag</span>
        <p>Address<br>Second Line</p>
        <i class="secondary-content material-icons small">gradegradegrade</i>
      </li>
      <li class="collection-item avatar">
        <img src="images/amag_profile_logo.png" alt="" class="circle">
        <span class="title">Amag</span>
        <p>Address<br>Second Line</p>
        <i class="secondary-content material-icons small">gradegradegrade</i>
      </li>
      <li class="collection-item avatar">
        <img src="images/amag_profile_logo.png" alt="" class="circle">
        <span class="title">Amag</span>
        <p>Address<br>Second Line</p>
        <i class="secondary-content material-icons small">gradegradegrade</i>
      </li>
      <li class="collection-item avatar">
        <img src="images/amag_profile_logo.png" alt="" class="circle">
        <span class="title">Amag</span>
        <p>Address<br>Second Line</p>
        <p class="grey-text secondary-content">No rating</p>
      </li>
      <li class="collection-item avatar">
        <img src="images/amag_profile_logo.png" alt="" class="circle">
        <span class="title">Amag</span>
        <p>Address<br>Second Line</p>
        <i class="secondary-content material-icons small">gradegradegrade</i>
      </li>
      <li class="collection-item avatar">
        <img src="images/amag_profile_logo.png" alt="" class="circle">
        <span class="title">Amag</span>
        <p>Address<br>Second Line</p>
        <i class="secondary-content material-icons small">gradegradegrade</i>
      </li>
      <li class="collection-item avatar">
        <img src="images/amag_profile_logo.png" alt="" class="circle">
        <span class="title">Amag</span>
        <p>Address<br>Second Line</p>
        <i class="secondary-content material-icons small">gradegradegrade</i>
      </li>
      <li class="collection-item avatar">
        <img src="images/amag_profile_logo.png" alt="" class="circle">
        <span class="title">Amag</span>
        <p>Address<br>Second Line</p>
        <p class="grey-text secondary-content">No rating</p>
      </li>
  </ul>

</div>


<!-- Section Partners -->
<div class="container partners-container">
  <h5 class="light">Charhelper trusted partners</h5>
  <div class="partners row">
    <i class="col s3 m2 icon-amag"></i>
    <i class="col s3 m2 icon-bosch"></i>
    <i class="col s3 m2 icon-amag"></i>
    <i class="col s3 m2 icon-garage-plus"></i>
    <i class="col s6 m2 icon-amag"></i>
    <i class="col s6 m2 icon-garage-plus"></i>
  </div>
</div>
<?php include 'footer.php';?>
