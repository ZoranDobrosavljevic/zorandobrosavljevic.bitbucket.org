
<!-- Login modal -->
<div class="row">
   <div id="login-modal" class="register-login-modal modal offset-s1 col s10 m6 offset-m3 l4 offset-l4">
     <div class="col s12 modal-content">
       <h4>Join us</h4>
       <p>Why should you register, blabla.</p>
		 </div>
			 <div class="col s12 divider"></div>
		<div class="col s12 modal-content">
			 <div class="input-field"><input id="login-email" type="email"><label for="login-email">Your email</label></div>
			 <div class="input-field"><input id="login-password" type="password"><label for="login-password">Your password</label></div>
			 <p class="col s6 rememberme thin"><input type="checkbox" id="rememberme" checked="" /><label for="rememberme">Remember me</label></p>
			 <div class="btn col s6">Login</div>
			 <a class="col s12 center light">Forgot Your password?</a>
			 <a class="col s12 center light register-modal-trigger modal-close" href="#register-modal">Don't have an account? Register here!</a>
		 </div>
		 <div class="col s12 divider"></div>
		 <div class="modal-content">
       <p class="col s12 center">or use social login</p>
			 <div class="btn col s12 offset-l2 l8 social-login-btn" style="background: #3b5998"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</div>
			 <div class="btn col s12 offset-l2 l8 social-login-btn" style="background: #D34836"><i class="fa fa-google-plus" aria-hidden="true"></i> Google</div>
			 <div class="btn col s12 offset-l2 l8 social-login-btn" style="background: #0077B5"><i class="fa fa-linkedin" aria-hidden="true"></i> LinkedIn</div>
     </div>
   </div>
</div>

<!-- Register modal -->
<div class="row">
   <div id="register-modal" class="register-login-modal modal offset-s1 col s10 m6 offset-m3 l4 offset-l4">
     <div class="col s12 modal-content">
       <h4>Register now</h4>
       <p>Why should you register, blabla.</p>
		 </div>
			 <div class="col s12 divider"></div>
		<div class="col s12 modal-content">
			<div class="input-field"><input id="register-name" type="text"><label for="register-name">Your name</label></div>
			<div class="input-field"><input id="register-email" type="email"><label for="register-email">Your email</label></div>
			<div class="input-field"><input id="register-password" type="password"><label for="register-password">Enter password</label></div>
			<div class="input-field"><input id="register-password-comfirm" type="password"><label for="register-password-comfirm">Enter password</label></div>
			<div class="btn col s6 right">Register</div>
		 </div>
   </div>
</div>

<div id="floating-contant-btn" class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
	<a href="#feedback-form-wrapper"class="btn-floating btn-large">
  	<i class="fa fa-envelope-o" aria-hidden="true"></i>
  </a>
</div>

<footer id="footer" class="page-footer thin">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h4 class="white-text icon-logo"></h4>
        <h5 class="thin white-text">Worauf wartest du?</h5>
        <p class="grey-text text-lighten-4">Das Leben ist zu kurz um ständig bei Garagen anzurufen...oder noch schlimmer bei allen vorbeizugehen um nach den Kosten zu fragen! Zurücklehnen, Kaffee trinken (oder noch besser ein kühles Bier) und mit ein paar Klicks holst du dir deine Offerten. Klingt das nicht besser?</p>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
      © 2016 Carhelper AG <a class="white-text text-lighten-3" href="">Datenschutzrichtlinie</a>
    </div>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/carhelper.js"></script>
</body>
</html>
