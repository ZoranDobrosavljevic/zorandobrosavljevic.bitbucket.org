function formClick(button){
  document.querySelector('#form').classList.add(button);
}

document.querySelector('.edit-step2').addEventListener('click', backToStep2, false);

function backToStep2(){
  document.querySelector('#form').classList.remove('show31', 'show4', 'show5');
}

document.querySelector('.edit-step3').addEventListener('click', backToStep3, false);

function backToStep3(){
  document.querySelector('#form').classList.remove('show4', 'show5');
}
