(function($){
  $(function(){
    $('.button-collapse').sideNav();
    $('.parallax').parallax();
  });
})(jQuery);

$(document).scroll(function(){
  if (($(window).scrollTop() > 130)|| ($('body').hasClass('formOpened'))){
    $('#nav-transparent').removeClass('nav-transparent');
  } else {
    $('#nav-transparent').addClass('nav-transparent');
  };

    if ($(window).scrollTop() > 130){
      $('body').addClass('scroll');
    } else {
      $('body').removeClass('scroll');
    };
  });


function toggleServiceOptions(){
  $('.service-options-header').velocity("slideUp", { duration: 300 });
  $('.service-options').velocity("slideDown", { duration: 300 });
};

function toggleServiceForm(type){
    $('.service-options').velocity("slideUp", { duration: 300 });
    $('.service-form').velocity("slideDown", { duration: 300 });
    $('.service-form ').addClass('type-' + type);
    // $('.service-form-header .icon').removeClass().addClass('icon-service-'+ type);
    $('body').toggleClass('formOpened');
    $('#nav-transparent').removeClass('nav-transparent');
};

function closeForm(){
    $('.service-options').velocity("slideDown", { duration: 300 });
    $('.service-form').velocity("slideUp", { duration: 300 });
    $('.service-form').attr('class',
                 function(i, c){
                    return c.replace(/(^|\s)step-\S+/g, '');
                 });
     $('.service-form').attr('class',
                  function(i, c){
                     return c.replace(/(^|\s)type-\S+/g, '');
                  });
    $('.service-form').removeClass("type*");
    $('body').toggleClass('formOpened');
    $('#nav-transparent').addClass('nav-transparent');
    $(".service-form input, textarea").val("");
    $(".service-form input").not(".included-service").attr('checked', false);
    $('.service-form input, textarea').not(".included-service").removeAttr('disabled');
};

function required(){
  console.log('cek');
  if ($('.step-3 :input').hasvalue == ""){
    console.log('prazno');
  }
};

$(document).ready(function(){
  $('.tooltipped').tooltip({delay: 50});
  var navHeight = $('nav').height();
  $('.login-modal-trigger').leanModal();
  $('.register-modal-trigger').leanModal();
  $('.video-how-it-works-modal-trigger').leanModal();
  $('.make-offer-trigger').leanModal();
  $('.add-extra-works-trigger').leanModal();



  $('.messages-list').scrollTop($('.messages-list').prop("scrollHeight"));

  $("#floating-contant-btn").click(function() {
    $('html, body').animate({
        scrollTop: $("#feedback-form-wrapper").offset().top
    }, 500);
    $('#feedback-form').toggle('hide');
    $('#feedback-btn').text(function(i, text){
      return text === "Senden" ? "Feedback abgeben" : "Senden";
    });
});

  $('.datepicker').pickadate({
    closeOnSelect: true
  });
  $('#feedback-btn').on('click', function(event) {
       $('#feedback-form').toggle('hide');
       $(this).text(function(i, text){
         return text === "Senden" ? "Feedback abgeben" : "Senden";
       })
  });
  $('#toggle-service-request-list').on('click', function(event) {
       $('#service-resuest-user-pick').toggle('hide');
       $(this).text(function(i, text){
         return text === "Hide list" ? "Show list" : "Hide list";
       })
  });
  $('#service-form-btn').on('click', function(event) {
    var step = $("#service-form").attr("step");
    var navHeight = $('nav').height() + $('.service-form-header').height();
    if (!$('#service-form').hasClass('step-2')){
        $("#service-form").addClass("step-2");
        $('.one :input').attr('disabled', 'disabled');
        if (window.matchMedia('(max-width: 600px)').matches) {
          $('body').animate({
            scrollTop: $(".two").offset().top - 155
          }, 500);
        };
    }else{
      $("#service-form").addClass("step-3");
      required();
      $('.two :input').attr('disabled', 'disabled');
      if (window.matchMedia('(max-width: 600px)').matches) {
        $('html, body').animate({
          scrollTop: $(".three").offset().top - 155
        }, 500);
      };
    };
  });

  $('.garage-list-toggle').on('click', function(event){
    $('.user-pick').toggleClass('hide');
    $('.auto-pick').toggleClass('hide');
  });
  $('#addExtraWorksBtn').on('click', function(event) {
       $('#extra-works').toggle('hide');
       $('#addExtraWorksBtn').toggle('hide');
  });
  $('.garage').on('click', function(event) {
       $(this).toggleClass('selected');
  });

  $('.one').on('click', function(event){
    if ($('#service-form').hasClass('step-2')) {
      $('#service-form').removeClass('step-2');
      $('.one :input').removeAttr('disabled');
      $('#service-form').removeClass('step-3');
      $('.two :input').removeAttr('disabled');
    };
  });

  $('.two').on('click', function(event){
    if ($('#service-form').hasClass('step-3')) {
      $('#service-form').removeClass('step-3');
      $('.two :input').removeAttr('disabled');
    };
  });
});
