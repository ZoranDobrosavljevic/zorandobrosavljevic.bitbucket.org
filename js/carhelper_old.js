/*
     * Replace all SVG images with inline SVG
     */
        // jQuery('img.svg').each(function(){
        //     var $img = jQuery(this);
        //     var imgID = $img.attr('id');
        //     var imgClass = $img.attr('class');
        //     var imgURL = $img.attr('src');
        //
        //     jQuery.get(imgURL, function(data) {
        //         // Get the SVG tag, ignore the rest
        //         var $svg = jQuery(data).find('svg');
        //
        //         // Add replaced image's ID to the new SVG
        //         if(typeof imgID !== 'undefined') {
        //             $svg = $svg.attr('id', imgID);
        //         }
        //         // Add replaced image's classes to the new SVG
        //         if(typeof imgClass !== 'undefined') {
        //             $svg = $svg.attr('class', imgClass+' replaced-svg');
        //         }
        //
        //         // Remove any invalid XML tags as per http://validator.w3.org
        //         $svg = $svg.removeAttr('xmlns:a');
        //
        //         // Replace image with new SVG
        //         $img.replaceWith($svg);
        //
        //     }, 'xml');
        //
        // });


        $(document).scroll(function(){
          if (($(window).scrollTop() > 130)|| ($('body').hasClass('formOpened'))){
            $('#nav-transparent').addClass('nav-orange');
            $('#nav-transparent').addClass('z-depth-1');
            $('#nav-transparent').removeClass('nav-transparent');

          } else {
            $('#nav-transparent').removeClass('nav-orange');
            $('#nav-transparent').removeClass('z-depth-1');
            $('#nav-transparent').addClass('nav-transparent');
            }
          });

//



// var circle = document.querySelector('.menu-btn');
// // var link = document.querySelector('.menu-content').querySelectorAll('li');  odkoment
// var ham = document.querySelector('.menu-hamburger');
// var main = document.querySelector('main');
// var win = window;
// // var body = document.querySelector('body');
//
// function openMenu(event) {
// circle.classList.toggle('active');
// ham.classList.toggle('menu-close');
// main.classList.toggle('active');
// // body.classList.toggle("overflow-hidden");
// for (var i = 0; i < link.length; i++) {
//   link[i].classList.toggle('active');
// }
// event.preventDefault();
// event.stopImmediatePropagation();
// }
//
// function closeMenu() {
// if (circle.classList.contains('active')) {
//   circle.classList.remove('active');
//   body.classList.remove("overflow-hidden");
//   for (var i = 0; i < link.length; i++) {
//     link[i].classList.toggle('active');
//   }
//   ham.classList.remove('menu-close');
//   // main.classList.remove('active');
//
// }
// }
//
// // circle.addEventListener('click', openMenu, false); odkoment
//
// win.addEventListener('click', closeMenu, false);
//
//
// var getOffers = document.querySelector('#cta-button');
// var landing = document.querySelector('#landing');
// var getEasyServiceBtn = document.querySelector('#getEasyService');
//
// function openOffers(){
//   if (landing.classList.contains('landingOffers')) {}
//   else{
//     getOffers.classList.add('offersShown');
//     landing.classList.add('landingOffers');
//     $('html, body').animate({scrollTop: $('.offersShown').offset().top-150}, 300);
//   }
// }
//
// var ham = document.querySelector('.menu-hamburger');
//
// function getEasyService(){
//
//
// }
//
//
// getOffers.addEventListener('click', openOffers, false);
// // getEasyService.addEventListener('click', getEasyService, false);

// function toggleServiceOptions(){
//   $('#landing-page-text').toggle('show');
//   $('.service-options-item').toggle('show');
//   $('.open-form-btn').toggle('show');
//   $('#service-options-header-text').toggle('show');
//   $('#nav-transparent').removeClass('nav-transparent');
//
// };


function toggleServiceOptions(){

  $('.open-form-btn').animate({
    height: "toggle"
    },{
        duration: 100,
        specialEasing: {
          height: "swing"
        },
        complete: function() {
          $('.service-options-item').animate({
            height: "toggle"
          });
        }
      });
  $('#landing-page-text').animate({
    height: "toggle"
    },{
        duration: 100,
        specialEasing: {
          height: "swing"
        },
        complete: function() {
        }
      });
  $('#service-options-header-text').animate({
    height: "toggle"
  });
};

function toggleServiceForm(){
  $('.service-options-item').toggle('show');
  $('.service-form').toggle('show');
  $('#service-options-header-text').hide();
  $('body').toggleClass('formOpened');
  $('.disabled').removeClass('disabled');
    $('#nav-transparent').removeClass('nav-transparent');

  // $(this).text(function(i, text){
  //   return text === "Get 3 offers for servicing" ? "X Choose service you need:" : "Get 3 offers for servicing";
  // })
};

$(document).ready(function(){
  var navHeight = $('nav').height();
  $('.datepicker').pickadate({
  // selectMonths: true, // Creates a dropdown to control month
  // selectYears: 15 // Creates a dropdown of 15 years to control year
  closeOnSelect: true
});

  $('#feedback-btn').on('click', function(event) {
       $('#feedback-form').toggle('hide');
       $(this).text(function(i, text){
         return text === "Senden" ? "Feedback abgeben" : "Senden";
       })
  });
  $('#toggle-service-request-list').on('click', function(event) {
       $('#service-resuest-user-pick').toggle('hide');
       $(this).text(function(i, text){
         return text === "Hide list" ? "Show list" : "Hide list";
       })
  });


  $('#service-form-btn').on('click', function(event) {
    var step = $("#service-form").attr("step");
    var navHeight = $('nav').height() + $('.service-form-header').height();

    console.log(navHeight);
    switch (step){
      case "1":
        $("#service-form").attr("step","2");
        $('.one').addClass('disabled');
        $('#oilchanges').readOnly = true;
        $('.two').toggle("hide");
        $(this).children("a").text('Go to step 3');
        if (window.matchMedia('(max-width: 600px)').matches) {
          $('body').animate({
            scrollTop: $(".two").offset().top - 155
          }, 500);
        };
        break;
    case "2":
        $("#service-form").attr("step","3");
        $('.two').addClass('disabled');
        $(".two").children('input').disabled = true;
        $('.three').toggle("hide");
        $(this).children("a").text('Submit');
        if (window.matchMedia('(max-width: 600px)').matches) {
          $('html, body').animate({
            scrollTop: $(".three").offset().top - 155
          }, 500);
        };
        break;
    };
  });


  // $('#next').on('click', function(event) {
  //   var step = $(this).attr("step");
  //   $('.type:not(#type1)').hide();
  //   switch (step){
  //     case "1":
  //     $('#step2').toggle("hide");
  //      $(this).attr("step","2");
  //      $(this).children("a").text('Go to step 3');
  //      $('html, body').animate({
  //        scrollTop: $("#step2").offset().top - navHeight
  //      }, 500);
  //      break;
  //   case "2":
  //     $('#step3').toggle('hide');
  //     $(this).attr("step","3");
  //     $(this).children("a").text('Go to step 4');
  //     $('html, body').animate({
  //       scrollTop: $("#step3").offset().top - navHeight
  //     }, 500);
  //     break;
  //   case "3":
  //     $('#step4').toggle('hide');
  //     $(this).attr("step","4");
  //     $(this).children("a").text('Submit');
  //     $('html, body').animate({
  //       scrollTop: $("#step4").offset().top - navHeight
  //     }, 500);
  //     break;
  //   }
  // });

  $('#garage-list-toggle').on('click', function(event){
    $('.user-pick').toggle('hide');
    $('.auto-pick').toggle('hide');
    $(this).text(function(i, text){
      return text === "I want to choose myself" ? "Pick for me" : "I want to choose myself";
    })
  });


  $('#addExtraWorksBtn').on('click', function(event) {
       $('#extra-works').toggle('hide');
       $('#addExtraWorksBtn').toggle('hide');
  });

});
