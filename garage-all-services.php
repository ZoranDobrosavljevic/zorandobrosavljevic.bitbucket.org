<?php include 'header.php';?>

<!-- List of all garage services -->
<div class="container">
		<h5>All services</h5>
		<div class="collection">
				<a href="garage-new-service.html" class="collection-item">Audi A4 2004<span class="new badge"></span></a>
				<a href="garage-new-service.html" class="collection-item">Audi A4 2004<span class="new badge"></span></a>
				<a href="garage-extra-work.html" class="collection-item">BMW 320d 2004<span class="badge">For: Friday</span></a>
		    <a href="garage-extra-work.html" class="collection-item">Audi A4 2004<span class="badge">For: 21. June</span></a>
		    <a href="garage-extra-work.html" class="collection-item">Easy service - Scoda Octacia 2004<span class="badge">For: 21. June</span></a>
		    <a href="garage-extra-work.html" class="collection-item">Easy service - BMW 116d 2004<span class="badge">For: 21. June</span></a>
		  </div>
			<ul class="pagination center col s12">
				<li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
				<li class="active"><a href="#!">1</a></li>
				<li class="waves-effect"><a href="#!">2</a></li>
				<li class="waves-effect"><a href="#!">3</a></li>
				<li class="waves-effect"><a href="#!">4</a></li>
				<li class="waves-effect"><a href="#!">5</a></li>
				<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
			</ul>
</div>
<div class="divider"></div>
<?php include 'footer.php';?>
